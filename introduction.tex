\section{Introduction}

Today, composite materials are {finding increasing application in large commercial aircraft} and the first composite-dominated planes like the B-787 or A350 are being built. 
Traditionally, fibers within a layer have the same orientation, leading to constant stiffness properties. 
As manufacturing technology has evolved, for example with the advent of automated fiber placement machines, the fiber orientation of a layer can be varied continuously leading to varying stiffness properties that can be best tailored for the applied loads. 
These composites are called variable stiffness laminates (VSL) in the current work. 

One of the largest problems in optimizing VSL is taking manufacturability into account.
To do this, linearly varying fibre angles are used by many researchers which have given promising, manufacturable, results \cite{Lopes, GurdalTatting, HyerCurvi, vanCampenCompB, WeaverLinVarAero, postBucklingBaier}.
The use of linearly varying fiber angle per bay, for stiffened plates, has also been investigated, and again it has been shown that varying the fiber angles leads to better performance \cite{bathVLSLopt, BuckleStiffVSL}.
Direct parametrisation of the tow paths using Lagrangian polynomials \cite{LagPol,Gustavo14,Gustavo15} {Lobatto-Legendre polynomials} \cite{Gustavo16,Gustavo17}, {Bezier curves} \cite{Gustavo18,Gustavo20,CTS2}, {splines} \cite{Gustavo21,Gustavo22}, {B-splines surfaces} \cite{Gustavo23} {and NURBS (Non-Uniform Rational B-Splines)} \cite{optPaths} have also been used. 
{Constant curvature paths have been extensively applied for flat panels and for cylindrical and conical shells} \cite{Gustavo25,Gustavo26,Gustavo27,Gustavo28,Gustavo29}, {as the curvature constraint evaluation is simplified.}
Large, manufacturable, improvements in buckling load were found, but the result is dependent of the basis functions chosen \cite{optPaths, NLRbuckling, LagPol}.
Hence, the total potential of VSL has not been exploited due to the pre-specified set of possibilities. 
Furthermore, most methods assume that the fibers are shifted, meaning a choice has to be made whether gaps or overlaps are allowed during manufacturing \cite{Manufacturing}.
{For instance, gaps and overlaps are observed in the cylindrical shells manufactured by Wu et al.} \cite{Gustavo25} {and the flat plates manufactured by Tatting and G\"urdal} \cite{Gustavo1}.

Another approach that leads to manufacturable designs is to align the fibres in the direction of principal stress. 
This has been shown to reduce stress concentrations, and can also lead to reduced weight using the tailored fiber placement method \cite{TailoredMinStress, TFPOpt}. 
Using load paths, or a hybrid combination of load paths and principal stress direction has also been used to design VSLs \cite{ToshDesignManufTest}.
Continuous tow shearing is a new manufacturing method, leading to varying fiber angles without any gaps or overlaps, but with a thickness variation that is coupled with the change in fiber angle \cite{CTS1, CTS2}.
By using a genetic algorithm, coupled with a pattern-search algorithm, or using the infinite strip method, large improvements in structural performance have been shown \cite{CTSscitech15, BathInfStrip}.
A more comprehensive review of optimization strategies can be found in Ghiasi et al. \cite{ReviewVSL}

To exploit the possibilities of VSL fully, a three-step approach has been developed at Delft University of Technology. 
The first step is to find the optimal stiffness distribution in terms of the lamination parameters. 
This is discussed in detail in IJsselmuiden \cite{Ijsselmuiden,SciTech2014}.
The second step is to find the optimal manufacturable fibre angle distribution \cite{LPtoAngle,Setoodeh2,LPtoLP2}. 
The third step is to retrieve the fiber paths, discussed in Blom \cite{Blom}. 
A schematic overview of this approach is shown in Figure \ref{fig:threeStep}.

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=1\columnwidth]{threeStep.jpg}
		\caption{schematic overview of the three-step optimization approach \cite{Ijsselmuiden}}
		\label{fig:threeStep}
	\end{center}
\end{figure}

Another way to achieve varying mechanical properties is by changing the thickness over the structure, leading to variable thickness (VT) structures.
Thickness variation in a laminate is described using two variables: the ply drop location and the  ply drop order.
The most popular approach is to use an evolutionary algorithm, typically a genetic algorithm, to optimize the number of layers per 'patch', while also optimizing the  ply drop order and stacking sequence, limited to a discrete set of angles (e.g., 0\degree, $\pm45$\degree, and 90\degree).
This area of research is referred to as laminate blending \cite{GAply,Adams,BlendingIjsselmuiden,Stolpe,BlendingToropov} and assumes that potential ply drop locations (i.e., patch boundaries) are pre-specified by the user.
A technique where the fiber angle is not restricted to a discrete set has also been developed, however, no manufacturing constraints, limiting the change in fiber angle from one element to the next, are posed \cite{AeroTopVSL}. 

Other techniques where the ply drop locations are not pre-specified use continuous optimization. 
Shape optimization \cite{PhDDelgado} is used to determine the shape and hence ply coverage and ply drop locations of the different layers.
The optimization is performed using a level-set approach with fiber angles limited to a discrete set.
Another continuous method is the discrete material and thickness optimization method, where the fiber angles belong to a discrete set and fictitious density variables are used to select the ply angles at any given location.
This has been done for compliance and buckling optimization \cite{Lund,Lund2}.
For this method, also a thickness filter has been implemented to get to physically feasible designs \cite{Lund2015}.
Thickness optimization for buckling load under uni- and bi-axial compression has also been performed. 
This work showed that large improvement in buckling load could be made without affecting the in-plane stiffness\cite{Biggers}.

The easiest  ply drop orders are inner or outer blending, where the layers are dropped from the symmetry plane, or from the outside respectively.
To determine the optimal  ply drop order, guide-based designs can be used: \cite{Adams}
a stacking sequence for the thickest laminate, called the guide laminate, is defined and the number of layers per patch.
The stacking sequence is then derived by dropping layers from the inside or outside, depending whether inner or outer blending is used, from the guide laminate.
A method that offers more possible ply drop orders and takes into account industrial guidelines is using stacking sequence tables \cite{Irisarri2014}.
A ply drop order and guide laminate are optimized.
\hl{The outer layer is most often not dropped since this would create a defect on the outside, from where cracks or a delamination could start.}

One of the first variable stiffness plates was manufactured and tested by Hyer et al. \cite{HyerManufacture}, based on optimized designs that were adapted to allow manufacturing \cite{HyerLee}.
The fiber paths were chosen to go around the cut-out as can be seen in Figure \ref{fig:HyerManufacture}.
These fiber paths preserved continuity of the fibers going around the cut-out, trying to lead the load away from the cut-out. 
During testing, it was found that the buckling load increased.
However, since the paths at the side were not continuous, the tensile strength was reduced: fiber continuity of these fibers was not preserved.
Hence, the idea of leading the load away was shown to work in practice, but due to manufacturing reasons, the tensile strength decreased.

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=0.9\columnwidth]{HyerManufacture.jpg}
		\caption{Steered paths manufactured by Hyer et al. \cite{HyerManufacture}.}
		\label{fig:HyerManufacture}
	\end{center}
\end{figure}

When manufacturing panels with linearly varying fiber angles, a choice has to be made whether gaps or overlaps are allowed when shifting the tows: either gaps or overlaps appear, or the width of the tow has to be changed, as shown in Figure \ref{fig:GapOverlap}.
Both options were manufactured, and as a reference, a constant stiffness laminate (CSL) was manufactured.
All three types of panels were tested for buckling under compression and shear load \cite{Jegley,Jegley2}.
Under compression, the buckling load of the CSL was predicted quite accurately: the buckling load of panels with gaps was on average about $10$\% higher than predicted, the buckling load of the panels with overlaps was almost $50$\% higher than the predicted load.
This is the result of a combination of effects: due to curing some pre-stress appears, which has a positive effect on the buckling load.
Furthermore, the actual material properties could differ from the assumed properties, and the gaps and overlaps have not been taken into account \cite{Jegley}.
The buckling results under shear load were different: the buckling load was predicted accurately for both the panel with gaps and overlaps. 
The buckling load of the VS panels under shear is lower than that of the CS panels, because they were optimized for buckling under compression.
The failure load on the other hand is increasing \cite{Jegley2}.
These tests clearly show that VSLs lead to an actual increase in performance, not just to a theoretical one.

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=.8\columnwidth]{GapOverlap.jpg}
		\caption{Gaps and overlaps appearing when shifting \cite{CTS2}.}
		\label{fig:GapOverlap}
	\end{center}
\end{figure}

Some VSL cylinders have been built and both a modal and bending test was performed \cite{PhDBlom}.
The results for both the modal and bending tests showed good agreement between FEA and experiments, for both mode shape and modal frequency, as well as for the strain field.
It was noted that the load redistribution behaves as expected: the tension side carried a larger part of the load than the compressive side.
Furthermore, the maximum strain at the same load was significantly reduced: the compressive strain was $10$\% lower, the tensile strain even $35$\% compared to the CSL design.
This shows the potential when a strength-critical design is optimized using VSL: using a strain-based failure criterion, this reduction in strain is significant.
Hence, these tests again confirmed the possibilities to improve structural performance by using VSLs.

Another set of cylinders was built and a series of tests was done by Wu in cooperation with different co-workers.
The same VS cylinders were used for a series of tests \cite{WuCylinderDesign,WuCylinderManufacturing}: first pristine (i.e, as manufactured without any cut-outs) \cite{WuCylinder}, then making cut-outs in them \cite{WuCylinderCutout}, followed by large cut-outs \cite{WuCylinderLargeCutout}.
Two cylinders were manufactured and tested with the same fiber angle distribution. 
The only difference is that one cylinder is manufactured using an overlap strategy, the other using the gap strategy.
The difference in weight, for the pristine cylinders, is $27$\%.
The axial stiffness and buckling load, normalized with respect to the weight, is $28$\% and $78$\% higher for the cylinders made using the overlap strategy compared to the cylinders made using the gap-strategy respectively.
The experiments agree within $10$\% with FEA, suggesting that VS cylinders are less sensitive to imperfections than their CS counterparts \cite{WuCylinder}.

The cylinders were not damaged during the buckling test, and another set of tests was done, with a cut-out on one side scaled to represent a passenger door on a commercial aircraft \cite{WuCylinderCutout}.
%Although the cylinders were not designed with cut-outs in mind, the reduction in both axial stiffness and buckling load do not exhibit a large decrease: on average 93\% (94 and 91\% was measured) of axial stiffness, and 86\% (82 and 91\% was measured) of buckling load remains compared to the pristine cylinders.
\hl{Even though the cylinders were not designed to have a cut-out, the decrease in mechanical performance compared to the pristine (i.e., without cut-out) cylinder was not large: on average 93\% (94 and 91\% was measured) of axial stiffness, and 86\% (82 and 91\% was measured) of buckling load remains compared to the pristine cylinders.}
This shows the advantage that VS cylinders offer: the influence of cut-outs is small.
Furthermore, it is expected that this influence could be further reduced when they are accounted for during design \cite{WuCylinderCutout}.
It has to be noted that the cut-outs were made in the low-stiffness part of the cylinder, which could be part of the reason for the small influence.

The cylinders still did not show any sign of damage, so the size of the cut-out was increased to represent a cargo door on a commercial aircraft.
Increasing the size of the cut-out had a limited effect on the performance: on average still 91\% (92\% and 90\% was measured) of axial stiffness and 85\% (both 85\%) of buckling load was preserved compared to the pristine cylinders.
For the tests with cut-outs good agreement was found with the linear bifurcation buckling loads.
Since it is known that the buckling load of unstiffened CS cylinders and plates are sensitive to geometric imperfection recently, a numerical study on the influence of imperfections on the performance of VS cylinders has been performed \cite{WuCylinderImperfection}.
Results showed what was expected based on the previous results: the imperfections have a limited influence.
The reason is that the buckling always occurs in the same region: by varying the fibre angle a large difference in local stiffness is created. Hence, the location of the buckling is not influenced by imperfections. 
This implies that the load redistribution is hardly influenced by the imperfections, and thus the effect of these imperfections is small \cite{WuCylinderImperfection}.

Recently a thermoplastic VS wingbox was designed, manufactured and tested at the University of Limerick. 
This was a unified approach: the stiffeners and skin were connected in-situ without the need for secondary processes.
No CS counterpart was made, but finite element analyses showed a 14\% improvement in buckling load. 
The experimentally obtained buckling load matched the predictions very well \cite{SciTechGio,SciTechVin}.

In this paper three different manufacturing approaches are compared: variable stiffness, variable thickness and a combination of both.
As a reference a CS laminate is made and tested.
The VS, VT, and VSVT laminates are 15\% lighter than the CS laminate.
The problem formulation and optimization is discussed in section \ref{sec:optimisation}.
Next, the changes necessary for manufacturing and the manufacturing itself are discussed in section \ref{sec:manufacture}.
The tests are described in section \ref{sec:tests}.
A detailed discussion is provided in section \ref{sec:discussion}.
Finally, the paper is concludes with section \ref{sec:conclusion}.
