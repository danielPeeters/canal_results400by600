\section{Optimization} \label{sec:optimisation}

% Describe the optimization approach briefly, this has been published before, so a brief description is enough.

% Also state the problem and the aim of saving 15 \% of weight.

\subsection{Overview of the optimization method}

The optimization performed in the present work is a direct application of the optimization strategy previously published in \cite{Gustavo51}.
The strategy combines fiber steering optimization \cite{peeters_optimization_2015, SciTech2014}, ply drop location \cite{ECCM16} and ply drop order optimization using stacking sequence tables (SSTs) \cite{Irissari}.
The approach fits into the three-step optimization framework proposed by IJsselmuiden for VSLs and mentioned in the introduction of this paper (see Figure \ref{fig:threeStep}).
First, a structural optimization is performed using a homogenized representation of the laminates based on lamination parameters. 
The output is an idealized design defined by a thickness distribution and stiffness distribution over the structure.
Second, the optimal fiber angle and ply-by-ply material distributions are retrieved.
%Third, the tow paths are designed from the fiber angle distribution using a streamline analogy [Blom CST2010 41].
In \cite{Gustavo51}, the second optimization step is divided into two successive optimization phases, referred to as Step 2.1 and Step 2.2 respectively.
Both phases combine an evolutionary optimizer, for its efficiency in solving combinatorial problems and escaping local optima, and a gradient-based optimizer, for its efficiency in handling numerous design variables and constraints in continuous problems.
\hl{A schematic overview is shown in Figure} \ref{fig:overview}

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=.75\columnwidth]{strategy.pdf}
		\caption{Schematic overview of the optimization strategy.}
		\label{fig:overview}
	\end{center}
\end{figure}

Step 2.1, starts by a SST evolutionary optimization that aims at providing an initial set of variable-thickness straight-fiber designs closely matching the thickness and stiffness distributions of the idealized design issued from the first optimization step.
The evolutionary algorithm (EA) performs a Pareto optimization with two objectives to minimize: an estimate of the distance to the target stiffness distribution in membrane and its counterpart in terms of bending stiffness.
Each solution is defined by a guide laminate, a ply drop order and a distribution of ply numbers.
The obtained non-dominated solutions are subsequently improved using gradient-based optimization with respect to the objective and constraints of the first step structural optimization.
Starting from the ply-per-ply constant fiber orientation and the ply number distribution returned by the EA, the optimizer returns ply-per-ply fiber orientation distributions, corresponding to steered plies, and a  thickness continuous distribution over the structure.
The ply-drop order is fixed, however the optimization result depends on it since it is assumed that all plies likely to be dropped share the same thickness reduction at each point of the structure whereas plies that cannot be dropped keep their initial thickness that corresponds to the thickness of the base ply material.
Consequently, the optimization has to be repeated for all combinations of plies that cannot be dropped.
\hl{In the current work, it is assumed that the outer ply cannot be dropped.}
The outcome of Step 2.1 is a set of candidate designs with optimal thickness distribution and fiber angle distributions. However the ply drop locations are still not known.

Retrieval of the ply drop locations based on the thickness continuous distribution is performed in Step 2.2.
Evolutionary optimization and topology-like gradient-based optimization are closely intertwined.
A permutation evolutionary algorithm is used to optimize the ply-drop order within the laminate.
Each solution evaluation calls for a continuous optimization of the ply-drop location.
Topology-like optimization is performed using a fictitious density distribution for each ply, under a volume constraint.
At each point of the structure, the density of a ply is constrained with respect to the density of all other plies to comply with the imposed ply-drop order.
Plies that cannot be dropped are fixed just like in Step 2.1.
In all other plies, the densities are forced to converge to either zero or one, thus defining a clear topology for each ply.
The gradient-based optimizer alternates between ply-per-ply density optimization and updates of the fiber angle distribution in each ply.
The outcome of Step 2.2 is a steered VSVT laminate with optimized fiber angle distribution and optimal ply-drop location in each ply.
The overall strategy allows to handle both symmetry and balanced design guidelines as well as a steering constraint that tends to minimize the number of gaps and overlaps at global level and ensures a minimal curvature radius for the tows at local level. 

% Figure 1. Irisarri2016 ?

\subsection{Buckling maximization for rectangular panels under compression}

The optimization strategy has been applied to the buckling maximization of rectangular panels under compression.
The panels are assumed to be simply supported on their four edges and the loaded sides are considered to remain straight.
The panel dimensions are $600 \times 400$ mm.
Uniaxial compression is applied in the direction of the length of the panel, which also corresponds to the $0\degree$ direction in the following.
Two materials have been benchmarked in this study: Cytec PRISM TX1100 with EP2400 PRISM resin and Hexcel HiTape material with RTM6 resin.
The material in-plane orthotropic elastic properties have been assumed to be the same for both materials with $E_{11}$ = 154 GPa, $E_{22}$ = 10.8 GPa, $G_{12}$ = 4.02 GPa and $\nu_{12}$ = 0.317.
The ply thickness is 0.19 mm for the TW1100, while for the HiTape the thickness is 0.12 mm. 
Thus 40 layers of HiTape or 26 layers of TX1100 give the same total thickness of 4.94 mm.

Manufacturing tests using the AFP machine at NLR showed that the minimal turning radius required to avoid wrinkling is about 800 mm for the TX1100 and 400 mm for the HiTape.
Two benchmarks have been defined, one for each material.
A CS laminate and a VT straight-fiber laminate have been manufactured using TX1100 material.
A CS laminate, a VS laminate without any ply drop and a VSVT laminate have been manufactured using HiTape material \hl{since the smaller steering radius gives a larger improvement in the buckling load}.
\hl{Since the current industry standard often limits the possible fiber angles to $0\degree$, $\pm45\degree$, and $90\degree$, the benchmarks only consist of these angles.
For the best buckling performance, a lot of $\pm 45\degree$ plies are wanted, and it is best practice to put them next to each other.
As a constraint on the compliance is imposed during the optimization, this constraint is posed to the benchmarks as well.
Hence, some $0\degree$ plies are added: 6 for the HiTape material and 4 for the TX1100 were found to be necessary.
To satisfy the 10\% rule also a $90\degree$ ply has to be included.
As a final constraint, no $0\degree$ and $90\degree$ ply should be put next to each other.}
The lay-up chosen for the TX1100 CS laminate is [45 -45 0 0 45 -45 90 45 -45 0 45 -45 0]$_s$ while the lay-up chosen for the HiTape CS laminate is [45 -45 0 0 45 -45 90 90 45 -45 0 0 45 -45 0 0 45 -45 90 90]$_s$.
The expected buckling loads are 71.8 kN and 71.1 kN respectively. 
Both reference CS designs are very close together such that all designs can be compared to each other.

For the VT, VS and VSVT laminates, the design objective is to save 15\% of weight with respect to the reference CS laminates.
\hl{Thus, by having the VS, VT and VSVT panels of the same weight, the results can easily be compared to each other.}
%\hl{It was opted to save 15\% weight rather than as much weight as possible due to the model used: the gaps and overlaps in VS and VT have an unknown influence, the resin rich areas in VT and VSVT have a negative influence, the pre-stress in VS and VSVT has a positive effect on the buckling load.
%By having the VS, VT and VSVT panels of the same weight, the results can easily be compared to each other.}
Hence, the VS laminate has been designed with 34 plies.
For the VT and VSVT laminates, the volume constraint has been chosen such that the weight is 15\% lower than the reference panels.
All three laminate configurations have been optimized for buckling.
They all share the following design constraints.
\hl{The global in-plane stiffness of the panel has to be at least the stiffness of the ``black aluminum'' constant thickness laminate with the same weight.
In this paper, the ``black aluminum'' reference laminate is a quasi-isotropic design with all lamination parameters equal to zero, hence no stacking sequence is associated with it.}
%The global in-plane stiffness of the panel has to be at least the stiffness of the ``black aluminium'' quasi-isotropic constant thickness laminate with the same weight.
%\hl{A quasi-isotropic design is in this paper defined as all in- and out-of-plane lamination parameters equal to zero, hence no stacking sequence is associated with it.}
%The outer plies are straight-fiber $\pm45^o$-plies in order to improve damage tolerance of the laminate.
The outer plies on the surfaces of the laminate and the inner plies on each side of the mid-plane of the laminate cannot be dropped.
%The inner plies on each side of the midplane are pairs of $0^o$ and $90^o$-plies in order to satisfy the so-called 10\% design guideline.
All laminates are symmetrical. 
Balance is imposed by stacking +$\theta$ and -$\theta$-plies together.

%The VT panel has been designed with a volume equivalent to a 22.1-ply panel, which correspond to 85\% of the 26 layers of the reference CS panel.
The VT panel has been designed with a volume equal to 85\% of volume of the 26-ply reference CS panel.
The minimum and maximum numbers of plies have been set to 12 plies and 28 plies respectively.
The buckling load estimated \hl{using a FE linear eigenvalue extraction analysis} is 85.2 kN.
%The buckling load estimated using linear FE buckling analysis, \hl{using a linear eigenvalue extraction analysis} is 85.2 kN.
It is remarkable that even though 15\% of weight is saved, the buckling load seems to improve with respect to the reference CS panel. 
The solution is detailed per pairs of $\pm\theta$-plies in Figure \ref{fig:VT}. 
Plies 1 to 14 are detailed only, with ply 1 being the outer ply and ply 14 the inner ply at the symmetry plane.
Note that although this panel satisfies the global stiffness constraint, it does not satisfy the 10\% design guideline, neither in its original form (at least 10\% of plies oriented at $0\degree$ , $\pm45\degree$, and $90\degree$ everywhere in the panel) nor in its lamination parameter reformulation \cite{abdalla_formulation_2009}, since the thinnest part of the panel is a pure $\pm45^o$-laminate.
\hl{This means that the optimization had a bit more freedom when compared to the VS or VSVT laminate since it does not need to satisfy the 10\% rule at every point.}

The VS laminate consists of 34 plies.
In order to enforce balance, the orientation of the ply at the midplane is set to $90\degree$.
After optimization, the obtained maximum buckling load is 60.2 kN, which corresponds to 15\% decrease, while purely based on thickness one would expect a 38\% decrease, since buckling scales with the thickness cubed.
Plies 1 to 17 are detailed per pairs of $\pm\theta$-plies in Figure \ref{fig:VS}. 
\hl{Ply 1 is the outer ply.}
The panel satisfies the 10\% design guideline everywhere in  the panel.

Finally, the VSVT panel has been optimized with a volume constraint equivalent to a constant thickness 34-ply panel.
The minimum and maximum numbers of plies have been set to 20 plies and 44 plies respectively.
The higher number of plies is due to the lower ply thickness in this case.
The inner plies close to the symmetry plane (the plies numbered 17 to 22) have been fixed to $0_2\degree$ , $\pm45\degree$ , and $90_2\degree$ to satisfy the 10\% design guideline.
The expected buckling load is 73.3 kN, slightly over the reference CS panel.
Plies 1 to 22 are detailed per pairs of $\pm\theta$-plies in Figure \ref{fig:VSVT}. 
\hl{The solid grey parts are the parts of the layer that have been dropped.}

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=1.\columnwidth]{VT_bw.pdf} % VT.jpg
		\caption{Straight fiber paths and ply coverage designed for the VT panel. The upper half of the laminate is detailed per balanced pairs of $\pm\theta$-plies. Areas where the plies are dropped are painted in grey color.}
		\label{fig:VT}
	\end{center}
\end{figure}

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=1.\columnwidth]{VS_bw.pdf} % VS.jpg
		\caption{Steered fiber paths designed for the VS panel. The upper half of the laminate is detailed per balanced pairs of $\pm\theta$-plies.}
		\label{fig:VS}
	\end{center}
\end{figure}

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=1.\columnwidth]{VSVT_bw.pdf} % VSVT.jpg
		\caption{Steered fiber paths and ply coverage  designed for the VSVT panel. The upper half of the laminate is detailed per balanced pairs of $\pm\theta$-plies. Areas where the plies are dropped are painted in grey color.}
		\label{fig:VSVT}
	\end{center}
\end{figure}